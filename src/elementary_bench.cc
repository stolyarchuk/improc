#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>
#include <new>
#include <random>
#include <span>

#include <immintrin.h>
#include <omp.h>

#include "benchmark/benchmark.h"

#include "aligned_allocator.h"

constexpr std::size_t kArraySize = 1280u * 1920u * 3;

// template <typename T>
// using aligned_vector = std::vector<T, boost::alignment::aligned_allocator<T, 32>>;

template <typename T>
using AlignedVector = std::vector<T, AlignedAllocator<T, 32>>;

void SqrtCpu(std::span<float> in, std::span<float> out) {
  for (std::size_t i = 0; i < kArraySize; ++i)
    out[i] = in[i] * in[i];
}

void SqrtCpuOmpParallel(std::span<float> in, std::span<float> out) {
#pragma omp parallel for
  for (std::size_t i = 0; i < kArraySize; ++i)
    out[i] = in[i] * in[i];
}

void SqrtCpuOmpSimd(std::span<float> in, std::span<float> out) {
#pragma omp simd
  for (std::size_t i = 0; i < kArraySize; ++i) {
    out[i] = in[i] * in[i];
  }
}

void SqrtCpuAvxU(std::span<float> in, std::span<float> out) {
  for (std::size_t i = 0; i < kArraySize; i += 8) {
    __m256 value = _mm256_loadu_ps(&in[i]);
    _mm256_storeu_ps(&out[i], _mm256_mul_ps(value, value));
  }
}

void SqrtCpuAvxUOmpParallel(std::span<float> in, std::span<float> out) {
#pragma omp parallel for
  for (std::size_t i = 0; i < kArraySize; i += 8) {
    __m256 value = _mm256_loadu_ps(&in[i]);
    _mm256_storeu_ps(&out[i], _mm256_mul_ps(value, value));
  }
}

void SqrtCpuAvx(std::span<float> in, std::span<float> out) {
  for (std::size_t i = 0; i < kArraySize; i += 8) {
    __m256 value = _mm256_load_ps(&in[i]);
    _mm256_store_ps(&out[i], _mm256_mul_ps(value, value));
  }
}

void SqrtCpuAvxOmpParallel(std::span<float> in, std::span<float> out) {
#pragma omp parallel for
  for (std::size_t i = 0; i < kArraySize; i += 8) {
    __m256 value = _mm256_load_ps(&in[i]);
    _mm256_store_ps(&out[i], _mm256_mul_ps(value, value));
  }
}

void SqrtCpuAvxD(std::span<float> in, std::span<float> out) {
  for (std::size_t i = 0; i < kArraySize; i += 8) {
    __m256 value = *(reinterpret_cast<__m256*>(&in[i]));
    _mm256_store_ps(&out[i], _mm256_mul_ps(value, value));
  }
}

void SqrtCpuAvxDOmpParallel(std::span<float> in, std::span<float> out) {
#pragma omp parallel for
  for (std::size_t i = 0; i < kArraySize; i += 8) {
    __m256 value = *(reinterpret_cast<__m256*>(&in[i]));
    _mm256_store_ps(&out[i], _mm256_mul_ps(value, value));
  }
}

template <typename V>
class Elementary : public benchmark::Fixture {
 public:
  Elementary() noexcept {
    in.resize(kArraySize);
    out.resize(kArraySize);

    std::random_device rnd_device{};
    std::mt19937 me{rnd_device()};
    std::uniform_real_distribution<float> dist(0, 99);

    std::generate(std::begin(in), std::end(in), [&dist, &me]() { return dist(me); });
  }

  ~Elementary() override = default;

  void SetUp(const ::benchmark::State& /* state */) override {}
  void TearDown(const ::benchmark::State&) override {}

  V in;
  V out;
};

using AlignedElementary = Elementary<AlignedVector<float>>;
using UnalignedElementary = Elementary<std::vector<float>>;

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpu)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpu(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpu)->Name("Elementary/SqrtCpu");

BENCHMARK_DEFINE_F(UnalignedElementary, SqrtCpu)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpu(in, out);
}
BENCHMARK_REGISTER_F(UnalignedElementary, SqrtCpu)->Name("Elementary/SqrtCpuU");

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpuOmpParallel)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuOmpParallel(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpuOmpParallel)->Name("Elementary/SqrtCpuOmpParallel");

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpuOmpSimd)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuOmpSimd(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpuOmpSimd)->Name("Elementary/SqrtCpuOmpSimd");

BENCHMARK_DEFINE_F(UnalignedElementary, SqrtCpuAvxU)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuAvxU(in, out);
}
BENCHMARK_REGISTER_F(UnalignedElementary, SqrtCpuAvxU)->Name("Elementary/SqrtCpuAvxU");

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpuAvx)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuAvx(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpuAvx)->Name("Elementary/SqrtCpuOmpSimd");

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpuAvxD)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuAvxD(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpuAvxD)->Name("Elementary/SqrtCpuOmpSimd");

BENCHMARK_DEFINE_F(UnalignedElementary, SqrtCpuAvxUOmpParallel)
(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuAvxUOmpParallel(in, out);
}
BENCHMARK_REGISTER_F(UnalignedElementary, SqrtCpuAvxU)->Name("Elementary/SqrtCpuAvxUOmpParallel");

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpuAvxOmpParallel)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuAvxOmpParallel(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpuAvxOmpParallel)->Name("Elementary/SqrtCpuOmpSimd");

BENCHMARK_DEFINE_F(AlignedElementary, SqrtCpuAvxDOmpParallel)(benchmark::State& state) {
  for (auto _ : state)
    SqrtCpuAvxDOmpParallel(in, out);
}
BENCHMARK_REGISTER_F(AlignedElementary, SqrtCpuAvxDOmpParallel)->Name("Elementary/SqrtCpuOmpSimd");
