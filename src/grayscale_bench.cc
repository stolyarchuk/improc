#include <opencv2/opencv.hpp>

#define SIMD_OPENCV_ENABLE 1

#include <immintrin.h>
#include <omp.h>

#include "Simd/SimdLib.hpp"
#include "benchmark/benchmark.h"

#include "aligned_allocator.h"
#include "allheaders.h"

static void Leptonica(benchmark::State& state) {
  auto* pixs = pixRead("/home/rs/stolyarch.uk/improc/images/0.jpg");
  PIX* out_img = nullptr;

  for (auto _ : state)
    out_img = pixConvertTo8(pixs, FALSE);

  pixWrite("/home/rs/stolyarch.uk/improc/images/0_leptonica.png", out_img, IFF_PNG);
}

BENCHMARK(Leptonica)->Name("GrayScale/Leptonica");

static void LeptonicaOmp(benchmark::State& state) {
  auto* pixs = pixRead("/home/rs/stolyarch.uk/improc/images/0.jpg");
  PIX* out_img = nullptr;

  for (auto _ : state)
    out_img = pixConvertTo8Omp(pixs, TRUE);

  pixWrite("/home/rs/stolyarch.uk/improc/images/0_leptonica_omp.png", out_img, IFF_PNG);
}

BENCHMARK(LeptonicaOmp)->Name("GrayScale/LeptonicaOmp");

static void GrayScaleOpenCV(benchmark::State& state) {
  cv::Mat src_image = cv::imread("/home/rs/stolyarch.uk/improc/images/0.jpg", cv::IMREAD_COLOR);
  cv::Mat dst_image;

  for (auto _ : state)
    cv::cvtColor(src_image, dst_image, cv::COLOR_BGR2GRAY);

  cv::imwrite("/home/rs/stolyarch.uk/improc/images/0_opencv.png", dst_image);
}

BENCHMARK(GrayScaleOpenCV)->Name("GrayScale/OpenCV");

static void GrayScaleSimdOmp(benchmark::State& state) noexcept {
  using View = Simd::View<Simd::Allocator>;

  View src_view;
  View dst_view;

  src_view.Load("/home/rs/stolyarch.uk/improc/images/0.jpg", View::Format::Bgr24);
  dst_view.Recreate(src_view.width, src_view.height, View::Format::Gray8);

  for (auto _ : state)
    Simd::BgrToGray(src_view, dst_view);

  dst_view.Save("/home/rs/stolyarch.uk/improc/images/0_simd.png", SimdImageFilePng);
}

BENCHMARK(GrayScaleSimdOmp)->Name("GrayScale/SimdOmp");

BENCHMARK_MAIN();
