#ifndef ALIGNED_ALLOCATOR_H
#define ALIGNED_ALLOCATOR_H

#include <cstddef>
#include <cstdlib>

template <std::size_t N>
struct IsAlignmentConstant : std::integral_constant<bool, (N > 0) && ((N & (N - 1)) == 0)> {};

template <typename T, std::size_t N = 32>
class AlignedAllocator {
  static_assert(IsAlignmentConstant<N>::value);

 public:
  using value_type = T;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using pointer = T*;
  using const_pointer = const T*;
  using reference = T&;
  using const_reference = const T&;

  AlignedAllocator() noexcept = default;
  ~AlignedAllocator() noexcept = default;

  template <typename U>
  explicit AlignedAllocator(const AlignedAllocator<U, N>&) noexcept {}

  inline pointer allocate(size_type size) {
    return static_cast<pointer>(std::aligned_alloc(N, size * sizeof(value_type)));
  }
  inline void deallocate(pointer ptr, size_type) { std::free(ptr); }
  inline void construct(pointer ptr, const value_type& wert) { new (ptr) value_type(wert); }
  inline void destroy(pointer ptr) { ptr->~value_type(); }

  [[nodiscard]] constexpr size_type max_size() const noexcept {
    return static_cast<size_type>(-1) / sizeof(value_type);
  }

  template <typename U>
  struct rebind {
    using other = AlignedAllocator<U, N>;
  };

  bool operator!=(const AlignedAllocator<T, N>&) const { return false; }
  bool operator==(const AlignedAllocator<T, N>&) const { return true; }
};

#endif
