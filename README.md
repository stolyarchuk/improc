# GrayScale Algorithms Comparision

![Screen Shot](/readme/benchmark.png)

## Tested algorithms

1. `Vanilla leptonica`
2. `Parallel leptonica`
3. `Vanilla OpenCV (SIMD+Parallel)`
4. `SIMD Lib (SIMD+Parallel)`

## Modified third party libraries

* [https://github.com/stolyarchuk/Simd](https://github.com/stolyarchuk/Simd)
* [https://github.com/stolyarchuk/leptonica](https://github.com/stolyarchuk/leptonica)

## Build & Run

* `git clone https://gitlab.com/stolyarchuk/improc --recurse-submodules`
* `mkdir build && cd build`
* `cmake -DBENCHMARK_DOWNLOAD_DEPENDENCIES=ON -DCMAKE_BUILD_TYPE=Release ..`
* `make -j`
